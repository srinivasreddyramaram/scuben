module SessionsHelper

	#Log in a user
	def log_in(user)
		session[:user_id] = user.id
	end

	# Returns current logged in user, if any
	def current_user
		if session[:user_id]
			@current_user = @current_user ||
			User.find_by(id: session[:user_id])
		end
	end

	# Returns boolean of a user login status
	def logged_in?
		!current_user.nil?
	end

	#invalidates the session, logging out the user
	def log_out
		session.delete(:user_id)
		@current_user= nil
	end
end
