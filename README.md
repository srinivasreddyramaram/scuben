#setting the path to ruby if .rb files don't build/run from sublime
sudo ln -s /home/user/.rbenv/shims/ruby /usr/local/bin/ruby


#postgresql connection:
sudo -u postgres createuser cnu -s
sudo -u postgres createdb cnudb
psql -d cnudb -U user -W
gem install pg
